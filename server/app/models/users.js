/*global require, unescape: true */
/**
 * Created by theotheu on 24-12-13.
 */
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    schema = mongoose.Schema;

/* Sub Schema definitions */
/* Sub-documents are docs with schemas of their own which are elements of a parents document array
 @see http://mongoosejs.com/docs/subdocs.html
 Nested documents differ from sub-documents by the fact that they can be defined with the schema and are not within in array.
*/
var groupSchema = schema({
    _id: {type: schema.Types.ObjectId, ref: "Group"}
});

/* Schema definitions */
// Schema types @see http://mongoosejs.com/docs/schematypes.html
var schemaName = schema({
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    groups: [groupSchema], // <----- sub document
    modificationDate: {type: Date, "default": Date.now}
}, {collection: "users"});

var modelName = "User";

module.exports = mongoose.model(modelName, schemaName);
