/*global require, module */
/**
 * Created by theotheu on 24-12-13.
 */

module.exports = function (app) {
    "use strict";

    /*  user routes
     ---------------
     We create a variable "user" that holds the controller object.
     We map the URL to a method in the created variable "user".
     In this example is a mapping for every CRUD action.
     */
    var controller = require('../app/controllers/products.js');

    // CREATE
    app.post('/products', controller.create);

    // RETRIEVE
    app.get('/products', controller.list);
    app.get('/products/:_id', controller.detail);

    // UPDATE
    app.put('/products/:_id', controller.update);

    // DELETE
    app.delete('/products/:_id', controller.delete);
};